# Dashboard with Bootstrap3


This dashboard is intended to use as a base for bootstrap3 templates.

## Requirements

To use it, install requirements file:

```bash
pip install -r dashboard_base/requirements.txt 
```
