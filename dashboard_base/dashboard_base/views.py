from django.views.generic import TemplateView

import settings


class BaseView(TemplateView):
    template_name = 'home.html'
