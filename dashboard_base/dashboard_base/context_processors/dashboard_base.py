from django.conf import settings


def dashboard_base(request):
    return {
        'BASE_CONTENT_PATH': getattr(settings, "BASE_CONTENT_PATH", "base_content.html")
    }